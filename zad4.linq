<Query Kind="SQL">
  <Connection>
    <ID>924a428b-f1df-4876-9352-07ad6175ad05</ID>
    <Persist>true</Persist>
    <Server>ODS-SMANDIC-N</Server>
    <Database>stuslu</Database>
  </Connection>
</Query>

SELECT ime,prezime,CAST(AVG(CAST(ocjena AS DECIMAL(3,2)))AS DECIMAL(3,2)) 'projecna ocjena' 
FROM ispit,student 
WHERE ocjena>1 AND ispit.mbrstud=student.mbr 
GROUP BY mbr,ime,prezime 
ORDER BY CAST(AVG(CAST(ocjena AS DECIMAL(3,2)))AS DECIMAL(3,2)) desc;

SELECT mbrstud,CAST(AVG(CAST(ocjena AS DECIMAL(3,2))) AS DECIMAL(3,2)) AS 'projecna ocjena' 
FROM ispit 
WHERE ocjena>1 
GROUP BY mbrstud 
HAVING CAST(AVG(CAST(ocjena AS DECIMAL(3,2))) AS DECIMAL(10,2))>2.5 

